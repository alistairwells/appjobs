<?php

namespace Domain\Document;

use App\Domain\Document\XmlDocument;
use ErrorException;
use Tests\TestCase;
use XMLReader;

class XmlDocumentTest extends TestCase
{
    public function testWhenAValidUrlIsPassedTheXmlFileCanBeOpened()
    {
        $document = (new XmlDocument(
            new XMLReader(),
            $this->getTestDataPath(self::class, 'sample_feeds/feed_1.xml'))
        )->processFirstRepeatingNode();

        $this->assertInstanceOf(XmlDocument::class, $document);
    }

    public function testWhenAnUnreachableUrlIsPassedThenThrowsAnErrorException()
    {
        $this->expectException(ErrorException::class);

        (new XmlDocument(
            new XMLReader(),
            $this->getTestDataPath(self::class, 'unreachable feed'))
        )->processFirstRepeatingNode();
    }

    public function getDataFindFirstRepeatingNodeName(): array
    {
        //Work around to expose Laravel helper methods
        $this->createApplication();

        return [
            'feed_1' => [$this->getTestDataPath(self::class, 'sample_feeds/feed_1.xml'), 'job'],
            'feed_2' => [$this->getTestDataPath(self::class, 'sample_feeds/feed_4.xml'), 'job'],
            'feed_3' => [$this->getTestDataPath(self::class, 'sample_feeds/feed_2.xml'), 'product'],
            'feed_4' => [$this->getTestDataPath(self::class, 'sample_feeds/feed_3.xml'), 'job'],
            'feed_5' => [$this->getTestDataPath(self::class, 'sample_feeds/feed_5.xml'), 'job'],
            'feed_6' => [$this->getTestDataPath(self::class, 'sample_feeds/feed_6.xml'), 'job'],
        ];
    }

    /**
     * @dataProvider getDataFindFirstRepeatingNodeName()
     */
    public function testWhenFindFirstRepeatingNodeNameIsCalledThenWeHaveTheExpectedResult($rawXml, $expectedNodeName)
    {
        $this->assertEquals($expectedNodeName, (new XmlDocument(
            new XMLReader(),
            $rawXml)
        )->processFirstRepeatingNode()->getFirstRepeatingNodeName());
    }

    public function getDataExtractFirstRepeatingNode(): array
    {
        //Work around as the Laravel helper methods are not available when the dataProvider is scaffolded
        $this->createApplication();
        $dataProvider = [];

        for ($i = 1; $i <= 6; $i++) {
            $dataProvider["feed_{$i}"] = [
                $this->getTestDataPath(self::class, "sample_feeds/feed_{$i}.xml"),
                $this->getTestDataPath(self::class, "first_nodes/feed_{$i}.xml"),
            ];
        }

        return $dataProvider;
    }

    /**
     * @dataProvider getDataExtractFirstRepeatingNode
     *
     * @param $rawXml
     * @param $expectedFirstNode
     */
    public function testWhenExtractFirstRepeatingNodeIsCalledThenWeHaveTheExpectedResult($rawXml, $expectedFirstNode)
    {
        $this->assertXmlStringEqualsXmlString(file_get_contents($expectedFirstNode),
            (new XmlDocument(
                new XMLReader(),
                $rawXml)
            )->processFirstRepeatingNode()->getFirstRepeatingNodeContent());
    }

    public function getDataOutput(): array
    {
        //Work around as the Laravel helper methods are not available when the dataProvider is scaffolded
        $this->createApplication();
        $dataProvider = [];

        for ($i = 1; $i <= 6; $i++) {
            $dataProvider["feed_{$i}"] = [
                $this->getTestDataPath(self::class, "sample_feeds/feed_{$i}.xml"),
                $this->getTestDataPath(self::class, "json_responses/feed_{$i}.json"),
            ];
        }

        return $dataProvider;
    }

    /**
     * @param $rawXml
     * @param $expectedJson
     * @dataProvider getDataOutput
     */
    public function testWhenValidXmlIsProcessedThenTheArrayStructureMatchesTheExpectedOutput($rawXml, $expectedJson)
    {
        $this->assertEquals(json_decode(file_get_contents($expectedJson), true),
            (new XmlDocument(
                new XMLReader(),
                $rawXml)
            )->processFirstRepeatingNode()->outputToArray());
    }

}


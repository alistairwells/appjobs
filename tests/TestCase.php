<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public function getTestDataPath(string $className, string $fileName): bool|string
    {
        $className = preg_replace('/^Tests/', '', $className);
        $className = preg_replace('/\\\\+/', '/', $className);

        return base_path('test_data') . '/'. $className . '/' . $fileName;
    }
}

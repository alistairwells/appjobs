<?php

namespace Tests\Feature;

use App\Http\Requests\ParseFirstNodeRequest;
use Mockery\MockInterface;
use Tests\TestCase;

class DocumentControllerTest extends TestCase
{
    public function testWhenInvalidXmlUrlIsPassedThenValidationThrowsError()
    {
        $this->postJson('/api/parse-first-node', [
            'url' => 'some invalid url',
        ])->assertStatus(422)->assertJsonValidationErrorFor('url');
    }


    public function getDataOutput(): array
    {
        //Work around as the Laravel helper methods are not available when the dataProvider is scaffolded
        $this->createApplication();
        $dataProvider = [];

        for ($i = 1; $i <= 6; $i++) {
            $dataProvider["feed_{$i}"] = [
                $this->getTestDataPath('', "Domain/Document/XmlDocumentTest/sample_feeds/feed_{$i}.xml"),
                $this->getTestDataPath('', "Domain/Document/XmlDocumentTest/json_responses/feed_{$i}.json"),
            ];
        }

        return $dataProvider;
    }

    /**
     * @dataProvider getDataOutput
     */
    public function testWhenValidXmlUrlIsPassedThenTheCorrectJsonIsReturned($rawXml, $expectedJson)
    {
        $this->mock(ParseFirstNodeRequest::class, function (MockInterface $mock) use ($rawXml) {
            $mock->shouldReceive('get')->with('url')->once()->andReturn($rawXml);
        });

        $this->postJson('/api/parse-first-node', [
            'url' => 'URL will be replace by above mock',
        ])->assertJson(json_decode(file_get_contents($expectedJson), true));
    }

}

<?php

use App\Http\Controllers\DocumentController;
use Illuminate\Support\Facades\Route;

Route::post('/parse-first-node', [DocumentController::class, 'parseFirstNode']);


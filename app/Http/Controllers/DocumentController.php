<?php

namespace App\Http\Controllers;

use App\Domain\Document\XmlDocument;
use App\Http\Requests\ParseFirstNodeRequest;
use Illuminate\Http\JsonResponse;
use XMLReader;

class DocumentController extends Controller
{
    public function parseFirstNode(ParseFirstNodeRequest $request): JsonResponse
    {
        $xmlDocument = new XmlDocument(new XMLReader(), $request->get('url'));

        return response()->json($xmlDocument->outputToArray());
    }
}

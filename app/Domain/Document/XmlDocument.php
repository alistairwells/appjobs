<?php

namespace App\Domain\Document;

use SimpleXMLElement;
use XMLReader;

class XmlDocument
{
    private XMLReader $xmlReader;
    private string $firstRepeatingNodeContent;
    private string $firstRepeatingNodeName;
    private string $url;

    public function __construct(XMLReader $xmlReader, string $url)
    {
        $this->xmlReader = $xmlReader;
        $this->url = $url;
    }

    public function getFirstRepeatingNodeName(): string
    {
        return $this->firstRepeatingNodeName;
    }

    public function getFirstRepeatingNodeContent(): string
    {
        return $this->firstRepeatingNodeContent;
    }

    /**
     * Read elements from the XML file stream until we detect a repeated element
     *
     * @return $this
     */
    private function findFirstRepeatingNodeName(): self
    {
        $processedNodes = [];

        $this->open();

        while ($this->xmlReader->read()) {
            if ($this->xmlReader->nodeType === XMLReader::ELEMENT) {
                if (in_array($this->xmlReader->name, $processedNodes)) {
                    $this->firstRepeatingNodeName = $this->xmlReader->name;
                    break;
                }
                $processedNodes[] = $this->xmlReader->name;
            }
        }

        $this->xmlReader->close();

        return $this;
    }

    /**
     * Extract the element matching $this->firstRepeatingNodeName
     *
     * @return $this
     */
    private function extractFirstRepeatingNode(): static
    {
        $this->open();

        while ($this->xmlReader->read()) {
            if ($this->xmlReader->nodeType === XMLReader::ELEMENT) {
                if ($this->xmlReader->name === $this->firstRepeatingNodeName) {
                    $this->firstRepeatingNodeContent = $this->xmlReader->readOuterXml();
                    break;
                }
            }
        }

        $this->xmlReader->close();

        return $this;
    }

    /**
     * Perform the full process of getting the first repeating node
     *
     * @return $this
     */
    public function processFirstRepeatingNode(): self
    {
        return $this->findFirstRepeatingNodeName()->extractFirstRepeatingNode();
    }

    /**
     * Output the modified data structure of the XML document as an array
     *
     * @return array
     */
    public function outputToArray(): array
    {
        $this->processFirstRepeatingNode()->decodeUrlEncodedCData();

        $arrayWrapper[$this->firstRepeatingNodeName] = json_decode(json_encode(simplexml_load_string(
            $this->firstRepeatingNodeContent,
            SimpleXMLElement::class, LIBXML_NOCDATA
        )), true);

        return $arrayWrapper;
    }

    private function decodeUrlEncodedCData(): void
    {
        $this->firstRepeatingNodeContent = str_replace(
            ['&lt;![CDATA[', ']]&gt;'],
            ['<![CDATA[', ']]>'],
            $this->getFirstRepeatingNodeContent()
        );
    }

    /**
     * Opens the XML file located at $this->url
     *
     * @return void
     */
    private function open(): void
    {
        $this->xmlReader->open($this->url);
    }
}

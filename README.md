## Hi Appjobs!

To get this demo up and running please perform the following from the command line:

1. Clone down this repository.
2. Run `composer install` from the directory the repo was cloned into
3. Run `php artisan key:generate` to set an application key
4. Run `php artisan:serve` to spin up a local web server to test against
5. Fire the following post request to http://127.0.0.1:8000/api/parse-first-node (replace server URL as appropriate and
   set header `Accept application/json`)

```json
{
    "url": "https://appjobs-general.s3.eu-west-1.amazonaws.com/test-xml-feeds/feed_1.xml"
}
```

### Running tests

Please run ``./vendor/bin/phpunit `` from the root directory to run the test suite

### Design considerations

- I am using the XMLReader class to read the remote files as a stream as they are quite chunky!
- I stream the file once to identify the first repeated node, and then again to extract this node. This is because
  XMLReader only can traverse forward and the internal file pointer cannot be reset. I did try caching the previous node
  to a variable which only required one traversal, but had concerns about memory usage if the document didn't have a
  repeating node for a long section.
- I used static data fixtures for my unit tests, where only 2 nodes from the larger files were left intact.
- I used the `LIBXML_NOCDATA` constant of SimpleXmlElement class to handle the CDATA, except for feed_6.xml which had URL encoded tags. I made the
  decodeUrlEncodedCData() function to convert these tags to the expected format for `LIBXML_NOCDATA`;
- URL validation is handled by the custom Request object ParseFirstNodeRequest

### Contact

Please feel free to reach out to me if there is any further questions. Many thanks

Alistair
